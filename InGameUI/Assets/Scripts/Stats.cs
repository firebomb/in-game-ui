﻿using TMPro;
using UnityEngine;
using System.Collections.Generic;
public class Stats : MonoBehaviour {

	public List<TMPro.TextMeshProUGUI> stats;

    private void OnEnable()
    {
        List<int> numericalStats = ExamplePlayerStats.playerStats.GetStats();
            for (int i =0; i<stats.Count; i++)
        {
            stats[i].SetText(numericalStats[i].ToString());
        }
    }

}
