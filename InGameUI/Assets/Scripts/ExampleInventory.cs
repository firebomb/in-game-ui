﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleInventory : MonoBehaviour {

    #region Simpleton
    public static ExampleInventory inventorySystem;

    private void Awake()
    {
        if (inventorySystem == null)
        {
            inventorySystem = this;
        }
    }
    #endregion
    public List<Item> inventory;

}
