﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EquipableManager : MonoBehaviour
{

    public List<RectTransform> sectors;
    private List<GameObject> inventoryItems = new List<GameObject>();
    [Space(20)]

    public Sprite BetterItem;
    public Sprite EqualItem;
    public Sprite WorseItem;

    [Space(20)]
    public List<CurrentItem> slots;

    [SerializeField]
    GameObject ItemPlaceholder;

    List<UIPlaceholderFill> AllPlaceholders;
    List<Item> items;
    private void OnEnable()
    {
        items = ExampleInventory.inventorySystem.inventory;

        AllPlaceholders = new List<UIPlaceholderFill>();

        RectTransform rect = GetComponent<RectTransform>();


        float height = 0;

        for (int i = 0; i < sectors.Count; i++)
        {
            height += sectors[i].sizeDelta.y;
        }
        for (int i = 0; i < items.Count; i++)
        {
            //Spawns the placeholder.
            GameObject placeholder = Instantiate(ItemPlaceholder, sectors[0].transform.position, Quaternion.identity);

            UIPlaceholderFill placeholderProperties = placeholder.GetComponent<UIPlaceholderFill>();

            //Sets the parent and the scale of the transform.
            placeholder.transform.SetParent(this.transform);
            placeholder.transform.localScale = Vector3.one;

            //sets the Item slot in UIPlaceholderFill to items[i] (items[i] are gained by attaining all the items
            //from the inventory class.)
            placeholderProperties.item = items[i];

            //Increase the height of the content by adding height of the placeholder object. 
            height += placeholder.GetComponent<RectTransform>().sizeDelta.y;


            //Sets the Manager reference so the item placeholder knows who is controlling him.
            placeholderProperties.Manager = this;


            //Sets the sibling number correctly.
            placeholder.transform.SetSiblingIndex(GetSiblingIndex(items[i]));

            //Compares Item With Equippable Item.
            CompareItem(placeholderProperties.item,
            placeholderProperties.Rating);

            AllPlaceholders.Add(placeholderProperties);

            inventoryItems.Add(placeholder);
        }
        //Sets the size of the content.
        rect.sizeDelta = new Vector2(rect.sizeDelta.x,
                                height);
    }
    void CompareAllItems()
    {
        for (int i =0; i<items.Count; i++)
        {
            CompareItem(items[i], AllPlaceholders[i].Rating);
        }
    }
    void CompareItem(Item item,Image image)
    {

        for (int i = 0; i<slots.Count; i++)
        {
            if (item.itemType == slots[i].TYPE)
            {
                if (Comparing(item,slots[i].currentItem) == 1)
                {
                    image.sprite = BetterItem;
                }
                else if (Comparing(item, slots[i].currentItem) == 0)
                {
                    image.sprite = EqualItem;
                }
                else
                {
                    image.sprite = WorseItem;
                }
            }
        }
    }
    private int Comparing(Item item, Item equipped)
    {
        if (item.TotalValueOfItem > equipped.TotalValueOfItem)
        {
            return 1;
        }
        else if (item.TotalValueOfItem == equipped.TotalValueOfItem)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }
    private int StatCheck(Item inventoryItem,Item equippedItem)
    {
        int statCounter = 0;

        statCounter += inventoryItem.HP - equippedItem.HP;
        statCounter += inventoryItem.MP - equippedItem.MP;
        statCounter += inventoryItem.SPD - equippedItem.SPD;

        return statCounter;
    }
    /// <summary>
    /// Returns Sibling number + 1. This is used for placing the item placeholders under their
    /// respected sorting areas.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    /// 
    private int GetSiblingIndex(Item item)
    {
        switch (item.itemType)
        {
            case Item.TypeOfItem.WEAPON:
                return sectors[0].transform.GetSiblingIndex() + 1;
            case Item.TypeOfItem.OFFHAND:
                return sectors[1].transform.GetSiblingIndex() + 1;
            case Item.TypeOfItem.ARMOR:
                return sectors[2].transform.GetSiblingIndex() + 1;
            case Item.TypeOfItem.RUNE:
                return sectors[3].transform.GetSiblingIndex() + 1;
        }
        return 0;
    }

    public void SwapMe(UIPlaceholderFill placeholder)
    {
        for (int i =0; i<slots.Count; i++)
        {
            if (placeholder.item.itemType == slots[i].TYPE)
            {
                Item placeholderItem = slots[i].currentItem;

                slots[i].currentItem = placeholder.item;
                placeholder.item = placeholderItem;

                CompareItem(placeholder.item, placeholder.Rating);

                CompareAllItems();

                return;
            }
        }
    }

    public void OnDisable()
    {
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                Destroy(inventoryItems[i]);
            }
            inventoryItems.Clear();
    }
}
