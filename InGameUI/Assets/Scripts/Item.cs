﻿using UnityEngine;
using System.Collections.Generic;


[CreateAssetMenu]
public class Item : ScriptableObject
{
    public enum TypeOfItem
    {
        WEAPON,
        OFFHAND,
        ARMOR,
        RUNE,
    }

    public string name;
    public string description;
    public Sprite Image;
    public TypeOfItem itemType;

    [Space(15)]
    public int HP, MP, SPD, ATK, DEF, LCK, MND, WILL;


    private int totalValueOfItem;

    public int TotalValueOfItem
    {
        get
        {
            return totalValueOfItem;
        }

        set
        {
            totalValueOfItem = value;
        }
    }


    public void Evaluate()
    {
        TotalValueOfItem = 0;
        TotalValueOfItem = TotalValueOfItem + HP + MP +
              SPD + ATK + DEF + LCK + MND + WILL;
    }
}