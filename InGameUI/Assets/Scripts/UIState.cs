﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UIState : MonoBehaviour {


    [SerializeField]
    private List<GameObject> panels;

	public enum MenuState
    {
        MAIN,
        STATUS,
        ITEMS,
        EQUIP,
        GEMS,
        JOURNAL,
        MAGIC,
        INFO
    }
    public MenuState currentState = MenuState.MAIN;

    private void Start()
    {
        OnPanelChange();
    }

    public void ChangeStateTo(string stateName)
    {
        switch(stateName)
        {
            case "Main":
                currentState = MenuState.MAIN;
                break;
            case "Status":
                currentState = MenuState.STATUS;
                break;
            case "Items":
                currentState = MenuState.ITEMS;
                break;
            case "Equip":
                currentState = MenuState.EQUIP;
                break;
            case "Gems":
                currentState = MenuState.GEMS;
                break;
            case "Journal":
                currentState = MenuState.JOURNAL;
                break;
            case "Magic":
                currentState = MenuState.MAGIC;
                break;
            case "Info":
                currentState = MenuState.INFO;
                break;
            default:
                currentState = MenuState.MAIN;
                break;
        }
        OnPanelChange();
    }
    private void OnPanelChange()
    {
        for (int i =0; i<panels.Count; i++)
        {

            if ((int)currentState == i)
            {
                panels[i].SetActive(true);
            }
            else
            {
                panels[i].SetActive(false);
            }
        }
    }
}
