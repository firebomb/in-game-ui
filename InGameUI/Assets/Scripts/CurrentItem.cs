﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentItem : MonoBehaviour {

    public Item currentItem;
    public Item.TypeOfItem TYPE;

    private void Awake()
    {
        currentItem.Evaluate();
        CheckIfLegit();

    }
    public void EquipItem(Item item)
    {
        currentItem = item;
        currentItem.Evaluate();
        CheckIfLegit();
    }
    private void CheckIfLegit()
    {
        if (currentItem.itemType != TYPE)
        {
            currentItem = null;
        }
    }
}

