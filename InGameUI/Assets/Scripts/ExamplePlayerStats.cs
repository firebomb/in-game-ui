﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExamplePlayerStats : MonoBehaviour
{
    #region Singleton
    public static ExamplePlayerStats playerStats;

    private void Awake()
    {
        if (playerStats == null)
        {
            playerStats = this;
        }
    }
    #endregion

    public int HP, MP, ATK,
        DEF, MND, WILL,
        LCK, SPD;
    
    public List<int> GetStats()
    {
        List<int> stats = new List<int>();

        stats.Add(HP);
        stats.Add(MP);
        stats.Add(ATK);
        stats.Add(DEF);
        stats.Add(MND);
        stats.Add(WILL);
        stats.Add(LCK);
        stats.Add(SPD);

        return stats;
    }
}
