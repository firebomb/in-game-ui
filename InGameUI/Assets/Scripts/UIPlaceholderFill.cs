﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode]
public class UIPlaceholderFill : MonoBehaviour {

    public Item item;
    [Space(30)]

    public Image Image;
    public Image Rating;

    public TMPro.TextMeshProUGUI Name;
    public TMPro.TextMeshProUGUI Description;

    EquipableManager manager;

    public EquipableManager Manager
    {
        get
        {
            return manager;
        }

        set
        {
            manager = value;
        }
    }



    private void Start()
    {
        SetProperties();
    }
   public void AttemptSwap()
    {
        Manager.SwapMe(this);
        SetProperties();
    }
    void SetProperties()
    {
        //Image.sprite = item.Image;
        Name.SetText(item.name);
        Description.SetText(item.description);

        item.Evaluate();
    }
}
